# Candidate Extraction in Two-stage Localization Models

Candidate Extraction in Two-stage Localization Models - implementation of RPN (Region Proposal Network), localization model proposed in Faster R-CNN. Implementation is in PyTorch deep learning framework.

My undergraduate thesis computer vision (deep learning, machine learning, artificial intelligence) project.

Project duration: Feb 2020 - July 2020.
