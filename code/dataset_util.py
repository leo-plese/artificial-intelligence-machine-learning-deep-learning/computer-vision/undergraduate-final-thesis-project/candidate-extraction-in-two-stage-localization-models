import image_util, xml_util, config
import numpy as np
from math import ceil

class Dataset():


    images = "/JPEGImages/"
    annotations = "/Annotations/"

    def __init__(self, ds_path, data_list, batch_size=None):
        self.path = ds_path
        self.batch_size = batch_size

        with open(ds_path + data_list) as f:
            lines = f.readlines()
        lines = np.array([x.strip() for x in lines])
        lines = [x for x in lines if len(x) > 0]

        self.data_names = lines

        self.new_epoch = True
        self.current_batch = 0
        if batch_size:
            self.total_batches = ceil(len(self.data_names) / self.batch_size)
        return

    def next_batch_names(self):
        np.random.seed(100)
        if self.new_epoch:
            np.random.shuffle(self.data_names)

        batch = self.data_names[self.current_batch * self.batch_size :
                                min((self.current_batch + 1) * self.batch_size, len(self.data_names))]

        if (self.current_batch + 1) * self.batch_size >= len(self.data_names):
            self.new_epoch = True
            self.current_batch = 0
        else:
            self.new_epoch = False
            self.current_batch += 1

        return batch

    def next_batch(self):
        img_names = self.next_batch_names()

        images = []
        boxes = []
        for i in range(len(img_names)):
            img_name = img_names[i]
            path1_img = self.path + self.images + img_name
            path1_ann = self.path + self.annotations + img_name.replace("jpg", "xml")

            img1 = image_util.load_image(path1_img)
            bboxes1 = xml_util.get_bboxes(path1_ann)
            images.append(img1)
            boxes.append(bboxes1)

        return np.array(images), np.array(boxes)

if __name__ ==  "__main__":
    ds = Dataset("..\\..\\"+config.DATASET_FILE_PATH, "\\train_list.txt",
                              16)
    #ds = Dataset("dataset/VOC2012", "/train_list.txt", 10)
    print(ds.total_batches)