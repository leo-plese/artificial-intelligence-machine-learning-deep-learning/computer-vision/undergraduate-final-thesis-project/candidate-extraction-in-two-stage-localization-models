import torch
import torch.nn as nn
import numpy as np
import xml_util, image_util, anchor_util, dataset_util, config
from torchvision import ops
import torch.nn.functional as F
from backbone import Resnet34_FPN, SamePad2d
import os

torch.manual_seed(20)


class RPN(nn.Module):

    def __init__(self, backbone, anchors_num):
        super().__init__()

        self.backbone = backbone
        self.anchors_per_pix = anchors_num

        self.conv_pad = SamePad2d(kernel_size=3, stride=1)
        self.conv = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3)

        self.relu = nn.ReLU()

        self.cls_conv = nn.Conv2d(in_channels=256, out_channels=self.anchors_per_pix * 2, kernel_size=1)

        self.cls_softmax = nn.Softmax(dim=2)

        self.bb_conv = nn.Conv2d(in_channels=256, out_channels=self.anchors_per_pix * 4, kernel_size=1)

    def forward(self, x):

        cls_softmaxes = []
        cls_scores = []
        bbox_deltas = []

        # for each FPN out = RPN in (feature maps P2-P6)
        for level in self.backbone.forward(x):
            y = self.conv_pad(level)
            y = self.conv(y)

            y = self.relu(y)

            fg_bg_conv = self.cls_conv(y)

            fg_bg = fg_bg_conv.permute(0, 2, 3, 1)
            fg_bg = fg_bg.contiguous()
            fg_bg = fg_bg.view(x.shape[0], -1, 2)
            cls_scores.append(fg_bg)

            fg_bg_softmax = self.cls_softmax(fg_bg)
            cls_softmaxes.append(fg_bg_softmax)

            bbox_conv = self.bb_conv(y)
            bbox = bbox_conv.permute(0, 2, 3, 1)
            bbox = bbox.contiguous()
            bbox = bbox.view(x.shape[0], -1, 4)
            bbox_deltas.append(bbox)

        cls_scores = torch.cat(cls_scores, 1)
        cls_softmaxes = torch.cat(cls_softmaxes, 1)
        bbox_deltas = torch.cat(bbox_deltas, 1)

        return cls_scores, cls_softmaxes, bbox_deltas

    # load model weights from filePth, or show message there is no weight file yet
    def load_weights(self, filePth, optimizer=None):
        if os.path.exists(filePth):
            checkpoint = torch.load(filePth)
            self.load_state_dict(checkpoint["model_state_dict"])
            if optimizer is not None:
                optimizer.load_state_dict(checkpoint["optimizer_state_dict"])
            print("OK - loaded weight file",filePth)
        else:
            print("No weight file!")
        self.train()
        return optimizer

    # get proposals with/without NMS applied
    def get_rpn_proposals(self, anchors, cls_softmaxes, bb_deltas, applyNms=False):
        cs = cls_softmaxes
        bs = bb_deltas
        vs = cs[:, 1]
        # get top BEST_N_PROPOSALS if no NMS, otherwise get all classification scores
        indices = torch.topk(torch.from_numpy(vs), len(vs) if applyNms else 2000)[1].numpy()
        #indices = torch.topk(torch.from_numpy(vs), config.BEST_N_PROPOSALS)[1].numpy()

        deltas = bs[indices]
        boxes = anchors[indices]

        # get anchor dimenions and x, y center coordinates
        height = boxes[:, 3] - boxes[:, 1]
        width = boxes[:, 2] - boxes[:, 0]
        center_y = boxes[:, 1] + 0.5 * height
        center_x = boxes[:, 0] + 0.5 * width

        # decode anchor dimenions and x, y center coordinates using deltas dx, dy, dw, dh
        center_y += deltas[:, 1] * height
        center_x += deltas[:, 0] * width
        height *= np.exp(deltas[:, 3])
        width *= np.exp(deltas[:, 2])

        # calculate new x and y min and max proposed anchor coordinates
        y1 = center_y - 0.5 * height
        x1 = center_x - 0.5 * width
        y2 = y1 + height
        x2 = x1 + width

        proposals = []
        for i in range(len(x1)):
            proposals.append([x1[i], y1[i], x2[i], y2[i]])

        # if NMS is to be applied, get BEST_N_PROPOSALS proposals given NMS_IOU_THRESHOLD for removing lower-scoring anchors compared to those higher-scoring ones
        if applyNms:
            proposals = np.array(proposals)
            indices = ops.nms(torch.from_numpy(proposals).float(), torch.from_numpy(vs[indices]).float(), config.NMS_IOU_THRESHOLD)[
                      :2000].numpy()
                      #:config.BEST_N_PROPOSALS].numpy()
            proposals = proposals[indices]

        return proposals

# calculate AR1000 for proposals gotten after NMS at each predefined IoU threshold
def calculate_ar_recalls_over_iou(boxes, boxes_gt):
    overlaps = anchor_util.calculate_overlaps(boxes, boxes_gt)

    positives_num = 0
    all_overlaps_gt = []
    overlaps_gt = torch.zeros(len(boxes_gt))

    positives_num += len(boxes_gt)

    for j in range(min(len(boxes), len(boxes_gt))):
        # determine proposal which  best covers each gt bbox (argmax) and get that IoU (max)
        argmax_overlaps, max_overlaps = np.argmax(overlaps, axis=0), np.max(overlaps, axis=0)

        # determined gt bbox which is best covered by any anchor (argmax) and get that IoU (max)
        ind_gt, overlap_gt = np.argmax(max_overlaps, axis=0), np.max(max_overlaps, axis=0)
        assert overlap_gt >= 0
        ind_box = argmax_overlaps[ind_gt]

        # store IoU coverage of the current gt bbox j
        overlaps_gt[j] = overlaps[ind_box, ind_gt]
        assert overlaps_gt[j] == overlap_gt

        # label the proposal box and the gt box as used
        overlaps[ind_box, :] = -1
        overlaps[:, ind_gt] = -1

    # append IoU overlap stored for each gt bbox (gotten in the above for loop)
    all_overlaps_gt.append(overlaps_gt)

    all_overlaps_gt = torch.cat(all_overlaps_gt, dim=0)
    all_overlaps_gt, _ = torch.sort(all_overlaps_gt)

    # get range of numbers in [AR_BOTTOM_IOU_THRESHOLD, AR_TOP_IOU_THRESHOLD] with step AR_IOU_STEP
    thresholds = torch.arange(config.AR_BOTTOM_IOU_THRESHOLD, config.AR_TOP_IOU_THRESHOLD + 1e-5, config.AR_IOU_STEP,
                              dtype=torch.float32)
    recalls = torch.zeros_like(thresholds)

    # recall at each threshold is: TP (those with IoU >= threshold) / number of gt bboxes
    for i, t in enumerate(thresholds):
        recalls[i] = (all_overlaps_gt >= t).float().sum() / positives_num

    return recalls

# calculate classification loss (for -1/1 loss)
def calc_rpn_class_loss(cls_gt, cls_logits):
    cls_gt = cls_gt.squeeze(axis=2)

    # do not include 0, only -1/1 classified anchors
    inds = np.where(cls_gt != 0)[1]

    # get positive gt bboxes
    cls_gt_selected = torch.tensor(cls_gt[0, inds] == 1, dtype=torch.long).cuda()
    cls_logits_selected = cls_logits[0, inds]

    # use cross entropy loss
    cls_loss = F.cross_entropy(cls_logits_selected, cls_gt_selected)

    return cls_loss

# calculate regression bounding box loss
def calc_rpn_bbox_loss(bbox_gt, cls_gt, bbox):
    cls_gt = cls_gt.squeeze(axis=2)

    # do not include -1/0, only 1 classified anchors
    inds = np.where(cls_gt == 1)[1]

    bbox_gt_selected = torch.tensor(bbox_gt[0, inds], dtype=torch.float32).cuda()
    bbox_selected = bbox[0, inds]

    # use smooth L1 loss
    bb_loss = F.smooth_l1_loss(bbox_selected, bbox_gt_selected)

    return bb_loss


if __name__ == "__main__":
    print("LR =",0.02)
    rpn = RPN(Resnet34_FPN(), 3)
    # rpn.cpu()
    rpn.cuda()

    trainable_params_no_batchnorm = [param for name, param in rpn.named_parameters() if
                                     param.requires_grad and "bn" not in name]
    trainable_params_only_batchnorm = [param for name, param in rpn.named_parameters() if
                                       param.requires_grad and "bn" in name]
    optimizer = torch.optim.SGD(
        params=[{"params": trainable_params_no_batchnorm, "weight_decay": 1e-4},
                {"params": trainable_params_only_batchnorm}],
        lr=0.02, momentum=0.9)
        #lr=0.001, momentum=0.9)

    anchors = anchor_util.get_anchors(config.IMAGE_SIZE, config.ANCHOR_SCALES, config.ANCHOR_RATIOS)

    train_batch_size = config.TRAIN_BATCH_SIZE
    valid_batch_size = config.VALID_BATCH_SIZE
    ds_train = dataset_util.Dataset("/content/gdrive/My Drive/dataset/VOC2012", "/train_list.txt", train_batch_size)
    ds_train_size = ds_train.total_batches
    ds_valid = dataset_util.Dataset("/content/gdrive/My Drive/dataset/VOC2012", "/valid_list.txt", valid_batch_size)
    ds_valid_size = ds_valid.total_batches


    weight_filenames = os.listdir("weights")
    from re import fullmatch

    # get last epoch number model was trained to
    epochs_sofar_num = [int(fullmatch(r"\D*(\d+)\.pth", xx).groups()[0]) for xx in weight_filenames]
    last_epoch_trained_num = max(epochs_sofar_num) if len(epochs_sofar_num) > 0 else 0
    start_epoch_num = last_epoch_trained_num
    end_epoch_num = start_epoch_num + config.EPOCHS_NUM

    optimizer = rpn.load_weights("/content/gdrive/My Drive/weights/rpnWts" + str(last_epoch_trained_num) + ".pth", optimizer)


    for epoch in range(start_epoch_num, end_epoch_num):
        print("Epoch", (epoch + 1))

        rpn_class_loss_avg = 0
        rpn_bbox_loss_avg = 0
        loss_avg = 0

        print("------------ TRAIN ------------")
        for i in range(ds_train.total_batches):
            optimizer.zero_grad()

            imgs, bboxes = ds_train.next_batch()
            cls_classes, cls_softmaxes, bb_deltas = rpn(imgs)

            rpn_cls, rpn_box_deltas = anchor_util.get_rpn_cls_and_box_deltas(train_batch_size, anchors, bboxes)

            rpn_class_loss = calc_rpn_class_loss(rpn_cls, cls_classes)
            rpn_bbox_loss = calc_rpn_bbox_loss(rpn_box_deltas, rpn_cls, bb_deltas)
            loss = rpn_class_loss + rpn_bbox_loss

            torch.cuda.empty_cache()
            # backprop
            loss.backward()

            optimizer.step()


            rpn_class_loss_avg += rpn_class_loss / ds_train_size
            rpn_bbox_loss_avg += rpn_bbox_loss / ds_train_size
            loss_avg += loss / ds_train_size

            print("Iter {}: rpn cls loss = {}, rpn bbox loss = {}, rpn total loss = {}".format(i, rpn_class_loss, rpn_bbox_loss,
                                                                                      loss))
            #print(rpn_class_loss_avg,rpn_bbox_loss_avg,loss_avg)
            print()


        with open("/content/gdrive/My Drive/losses/train_class_loss.txt","a") as train_class_loss_descr:
            train_class_loss_descr.write(str(rpn_class_loss_avg.cpu().detach().numpy())+"\n")

        with open("/content/gdrive/My Drive/losses/train_bbox_loss.txt", "a") as train_bbox_loss_descr:
            train_bbox_loss_descr.write(str(rpn_bbox_loss_avg.cpu().detach().numpy()) + "\n")

        with open("/content/gdrive/My Drive/losses/train_loss.txt", "a") as train_loss_descr:
            train_loss_descr.write(str(loss_avg.cpu().detach().numpy()) + "\n")

        #torch.save(rpn.state_dict(), "/content/gdrive/My Drive/weights/rpnWts" + str(1 + epoch) + ".pth")
        checkpoint = {"model_state_dict": rpn.state_dict(), "optimizer_state_dict": optimizer.state_dict()}
        torch.save(checkpoint, "/content/gdrive/My Drive/weights/rpnWts" + str(1 + epoch) + ".pth")

        print("++++++ LOSSES {} - T: ".format(epoch + 1))
        print(str(rpn_class_loss_avg.cpu().detach().numpy()))
        print(str(rpn_bbox_loss_avg.cpu().detach().numpy()))
        print(str(loss_avg.cpu().detach().numpy()))
        print()


        rpn_class_loss_avg = 0
        rpn_bbox_loss_avg = 0
        loss_avg = 0

        print("------------ VALID ------------")
        for i in range(ds_valid.total_batches):
            torch.cuda.empty_cache()

            imgs, bboxes = ds_valid.next_batch()
            cls_classes, cls_softmaxes, bb_deltas = rpn(imgs)

            rpn_cls, rpn_box_deltas = anchor_util.get_rpn_cls_and_box_deltas(valid_batch_size, anchors, bboxes)

            rpn_class_loss = calc_rpn_class_loss(rpn_cls, cls_classes)
            rpn_bbox_loss = calc_rpn_bbox_loss(rpn_box_deltas, rpn_cls, bb_deltas)
            loss = rpn_class_loss + rpn_bbox_loss


            rpn_class_loss_avg += rpn_class_loss.cpu().detach().numpy() / ds_valid_size
            rpn_bbox_loss_avg += rpn_bbox_loss.cpu().detach().numpy() / ds_valid_size
            loss_avg += loss.cpu().detach().numpy() / ds_valid_size

            print("Iter {}: rpn cls loss = {}, rpn bbox loss = {}, rpn total loss = {}".format(i, rpn_class_loss, rpn_bbox_loss,
                                                                                      loss))
            #print(rpn_class_loss_avg, rpn_bbox_loss_avg, loss_avg)
            print()

        with open("/content/gdrive/My Drive/losses/valid_class_loss.txt", "a") as valid_class_loss_descr:
            valid_class_loss_descr.write(str(rpn_class_loss_avg) + "\n")

        with open("/content/gdrive/My Drive/losses/valid_bbox_loss.txt", "a") as valid_bbox_loss_descr:
            valid_bbox_loss_descr.write(str(rpn_bbox_loss_avg) + "\n")

        with open("/content/gdrive/My Drive/losses/valid_loss.txt", "a") as valid_loss_descr:
            valid_loss_descr.write(str(loss_avg) + "\n")

        print("++++++ LOSSES {} - V: ".format(epoch + 1))
        print(str(rpn_class_loss_avg))
        print(str(rpn_bbox_loss_avg))
        print(str(loss_avg))
        print()

