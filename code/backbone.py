import torch
import torch.nn as nn
import math
import torch.nn.functional as F
import numpy as np
import config, image_util


# helper class taken from original open source Pytorch Mask RCNN implementation for padding feature maps
class SamePad2d(nn.Module):
    def __init__(self, kernel_size, stride):
        super(SamePad2d, self).__init__()
        self.kernel_size = torch.nn.modules.utils._pair(kernel_size)
        self.stride = torch.nn.modules.utils._pair(stride)

    def forward(self, inp):
        in_width = inp.size()[2]
        in_height = inp.size()[3]
        out_width = math.ceil(float(in_width) / float(self.stride[0]))
        out_height = math.ceil(float(in_height) / float(self.stride[1]))
        pad_along_width = ((out_width - 1) * self.stride[0] + self.kernel_size[0] - in_width)
        pad_along_height = ((out_height - 1) * self.stride[1] + self.kernel_size[1] - in_height)
        pad_left = math.floor(pad_along_width / 2)
        pad_top = math.floor(pad_along_height / 2)
        pad_right = pad_along_width - pad_left
        pad_bottom = pad_along_height - pad_top
        return F.pad(inp, [pad_left, pad_right, pad_top, pad_bottom], "constant", 0)


# Residual block - building block of ResNet-34 architecture
class ResidualBlock(nn.Module):

    def __init__(self, filters, kernel_size, channels_in, channels_out):
        super().__init__()

        self.relu = nn.ReLU()

        self.pad1 = SamePad2d(kernel_size=kernel_size, stride=1)
        self.c1 = nn.Conv2d(in_channels=filters, out_channels=filters, kernel_size=kernel_size)
        self.bn1 = nn.BatchNorm2d(filters, eps=0.001, momentum=0.01)

        self.pad2 = SamePad2d(kernel_size=kernel_size, stride=1)
        self.c2 = nn.Conv2d(in_channels=filters, out_channels=filters, kernel_size=kernel_size)
        self.bn2 = nn.BatchNorm2d(filters, eps=0.001, momentum=0.01)

        self.shortcut = self.shortcut_method(channels_in, channels_out)
        if isinstance(self.shortcut, nn.Conv2d):
            self.pad_shortcut = SamePad2d(kernel_size=1, stride=1)

    def shortcut_method(self, channels_in, channels_out):
        if channels_in != channels_out:
            return nn.Conv2d(in_channels=channels_out, out_channels=channels_out, kernel_size=1)
        else:
            return lambda x: x

    def forward(self, x):
        y = self.pad1(x)
        y = self.c1(y)
        y = self.bn1(y)
        y = self.relu(y)

        y = self.pad2(y)
        y = self.c2(y)
        y = self.bn2(y)
        y = self.relu(y)

        if isinstance(self.shortcut, nn.Conv2d):
            y_add = self.pad_shortcut(x)
            y += self.shortcut(y_add)
        else:
            y += self.shortcut(x)

        y = self.relu(y)

        return y


#  ResNet-34 architecture + FPN
class Resnet34_FPN(nn.Module):

    def __init__(self):
        super().__init__()

        self.relu = nn.ReLU()

        self.pad1 = SamePad2d(kernel_size=7, stride=2)
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=64, kernel_size=7, stride=2)

        self.bn1 = nn.BatchNorm2d(64, eps=0.001, momentum=0.01)
        self.pool1 = nn.Sequential(SamePad2d(kernel_size=3, stride=2), nn.MaxPool2d(kernel_size=3, stride=2))

        self.block2 = [ResidualBlock(64, 3, 64, 64).cuda() for _ in range(3)]
        self.conv2 = self.block2[-1].relu

        self.block3_pre_pad = SamePad2d(kernel_size=3, stride=2)
        self.block3_pre_conv = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, stride=2)
        self.block3 = [ResidualBlock(128, 3, 64, 128).cuda() for _ in range(4)]
        self.conv3 = self.block3[-1].relu

        self.block4_pre_pad = SamePad2d(kernel_size=3, stride=2)
        self.block4_pre_conv = nn.Conv2d(in_channels=128, out_channels=256, kernel_size=3, stride=2)
        self.block4 = [ResidualBlock(256, 3, 128, 256).cuda() for _ in range(6)]
        self.conv4 = self.block4[-1].relu

        self.block5_pre_pad = SamePad2d(kernel_size=3, stride=2)
        self.block5_pre_conv = nn.Conv2d(in_channels=256, out_channels=512, kernel_size=3, stride=2)
        self.block5 = [ResidualBlock(512, 3, 256, 512).cuda() for _ in range(3)]
        self.conv5 = self.block5[-1].relu

        self.M5_pad = SamePad2d(kernel_size=1, stride=1)
        self.M5 = nn.Conv2d(in_channels=512, out_channels=256, kernel_size=1, stride=1)
        self.P5_pad = SamePad2d(kernel_size=3, stride=1)
        self.P5 = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1)

        self.P6 = nn.MaxPool2d(kernel_size=1, stride=2)

        self.pre_M4_pad = SamePad2d(kernel_size=1, stride=1)
        self.pre_M4_conv = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=1, stride=1)

        self.P4_pad = SamePad2d(kernel_size=3, stride=1)
        self.P4 = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1)

        self.pre_M3_pad = SamePad2d(kernel_size=1, stride=1)
        self.pre_M3_conv = nn.Conv2d(in_channels=128, out_channels=256, kernel_size=1, stride=1)

        self.P3_pad = SamePad2d(kernel_size=3, stride=1)
        self.P3 = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1)

        self.pre_M2_pad = SamePad2d(kernel_size=1, stride=1)
        self.pre_M2_conv = nn.Conv2d(in_channels=64, out_channels=256, kernel_size=1, stride=1)

        self.P2_pad = SamePad2d(kernel_size=3, stride=1)
        self.P2 = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1)

    def forward(self, inp):

        inp = torch.from_numpy(inp).cuda()

        # permute in format which Conv2d accepts: [N H W C] -> [N C H W] (e.g. 1 512 512 3 -> 1 3 512 512)
        inp = inp.float().permute(0, 3, 1, 2)
        y = self.pad1(inp)
        y = self.conv1(y)

        y = self.bn1(y)

        y = self.relu(y)

        y = self.pool1(y)

        for b in self.block2:
            y = b(y)
        y2 = y

        y = self.block3_pre_pad(y)
        y = self.block3_pre_conv(y)

        for b in self.block3:
            y = b(y)
        y3 = y

        y = self.block4_pre_pad(y)
        y = self.block4_pre_conv(y)

        for b in self.block4:
            y = b(y)
        y4 = y

        y = self.block5_pre_pad(y)
        y = self.block5_pre_conv(y)

        for b in self.block5:
            y = b(y)

        m5 = self.M5(self.M5_pad(y))

        p5 = self.P5(self.P5_pad(m5))

        p6 = self.P6(p5)

        upsampled5 = F.upsample(m5, scale_factor=2, mode="nearest")

        m4 = self.pre_M4_conv(self.pre_M4_pad(y4)) + upsampled5

        p4 = self.P4(self.P4_pad(m4))

        upsampled4 = F.upsample(m4, scale_factor=2, mode="nearest")

        m3 = self.pre_M3_conv(self.pre_M3_pad(y3)) + upsampled4

        p3 = self.P3(self.P3_pad(m3))

        upsampled3 = F.upsample(m3, scale_factor=2, mode="nearest")

        m2 = self.pre_M2_conv(self.pre_M2_pad(y2)) + upsampled3

        p2 = self.P2(self.P2_pad(m2))

        return p2, p3, p4, p5, p6


if __name__ == "__main__":
    backbone = Resnet34_FPN()
    # backbone.cpu()
    backbone.cuda()

    path1 = "..\\" + config.DATASET_FILE_PATH + "\\JPEGImages\\" + config.TRAIN_IMAGE_INSTANCE_NAME + ".jpg"
    img1 = image_util.load_image(path1)
    x = np.array([img1])

    p2, p3, p4, p5, p6 = backbone(x)
    print(p2, p3, p4, p5, p6)
