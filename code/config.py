# root directory of Annotations and JPEGImages folders
DATASET_FILE_PATH = r"dataset\VOC2012"
# number of epochs for training an example
EPOCHS_NUM = 500
DATASET_EPOCHS_NUM = 100
TRAIN_BATCH_SIZE = 16
VALID_BATCH_SIZE = 4#8
LOSSES_FILE_PATH_BASE = "losses"
# directory to write RPN models into
WEIGHTS_FILE_PATH_BASE = r"weights\rpnWts"
# directory to read from the model from the epoch number EPOCHS_NUM (used in tests)
WEIGHTS_FILE_PATH = WEIGHTS_FILE_PATH_BASE + str(EPOCHS_NUM) + ".pth"
# current image instance for training (.jpg file)
TRAIN_IMAGE_INSTANCE_NAME = "2007_003195"   # image file name without extension (no ".jpg")

# dimensions widthxheight image is cut down to fit input to RPN
IMAGE_SIZE = (512, 512)
# anchor scales-areas used at each defined position
ANCHOR_SCALES = [64, 128, 256, 512, 1024]
# anchor ratios width/height
ANCHOR_RATIOS = [(1, 1), (1, 2), (2, 1)]

# for IoU >= POSITIVE_ANCHOR_THRESHOLD -> class 1 (object)
POSITIVE_ANCHOR_THRESHOLD = 0.6
# for IoU <= NEGATIVE_ANCHOR_THRESHOLD -> class -1 (background)
NEGATIVE_ANCHOR_THRESHOLD = 0.3
# number of sampled anchors
TRAIN_ANCHORS_PER_IMAGE = 128

# number of anchors selected before NMS application
BEFORE_NMS_SHOW_TOP_N = 100
# number of anchors selected after NMS application
AFTER_NMS_SHOW_TOP_N = 10
# number of best proposals (those with greatest classification score) to be taken in the end by NMS
BEST_N_PROPOSALS = 1000
# threshold for which NMS removes all the lower-scoring bounding boxes with IoU overlap higher than it with higher-scoring bounding boxes
NMS_IOU_THRESHOLD = 0.5
# bottom IoU threshold for AR1000 calculation
AR_BOTTOM_IOU_THRESHOLD = 0.5
# top IoU threshold for AR1000 calculation
AR_TOP_IOU_THRESHOLD = 0.95
# IoU threshold step in AR1000 calculation
AR_IOU_STEP = 0.05
VALIDATE_AR = False # False if printing AR in test5.py in not needed
