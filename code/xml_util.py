import xml.etree.ElementTree as ET
import numpy as np

# get bbox top-left (x1, y1) and bottom-right (x2, y2) coordinates from XML annotation file path
def get_bboxes(path):
    tree = ET.parse(path)
    root = tree.getroot()

    bboxes = []

    for boxes in root.iter('object'):

        for box in boxes.findall("bndbox"):
            x1 = round(float(box.find("xmin").text))
            y1 = round(float(box.find("ymin").text))
            x2 = round(float(box.find("xmax").text))
            y2 = round(float(box.find("ymax").text))

            bboxes.append([x1, y1, x2, y2])

    return np.array(bboxes)
