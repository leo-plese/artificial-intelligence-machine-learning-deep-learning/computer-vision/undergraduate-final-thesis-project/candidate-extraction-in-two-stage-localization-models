import numpy as np
from skimage import io
import config

# load image from path and set its size to IMAGE_SIZE
def load_image(path):
    image = io.imread(path)
    img_size = config.IMAGE_SIZE
    result_image = np.zeros((img_size[0], img_size[1], 3))
    result_image[:image.shape[0], :image.shape[1]] = image
    return result_image.astype("float32")
