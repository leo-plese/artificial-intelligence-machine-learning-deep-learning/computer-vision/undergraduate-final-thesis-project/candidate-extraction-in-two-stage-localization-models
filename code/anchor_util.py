import numpy as np
import config

# get all anchors for the picture
def get_anchors(img_dims, scales, ratios):
    anchors = []

    img_width, img_height = img_dims
    img_width //= 2
    img_height //= 2

    stride = 2

    # iterate over each anchor scale - corresponding to FPN levels
    for scale in scales:
        img_width //= 2
        img_height //= 2
        stride *= 2

        scale **= 2

        # go over each element in width/height at current FPN level
        for i in range(img_height):
            for j in range(img_width):
                # iterate over each (of current 3) anchor rations
                for ratio in ratios:
                    wr, hr = ratio

                    if wr == 1:
                        w = round(np.sqrt(scale / hr))
                        h = round(np.sqrt(scale * hr))
                    elif hr == 1:
                        w = round(np.sqrt(scale * wr))
                        h = round(np.sqrt(scale / wr))

                    anchors.append([j * stride - w / 2, i * stride - h / 2, j * stride + w / 2, i * stride + h / 2])

    return np.array(anchors)

# get classes and bounding box deltas for all images
def get_rpn_cls_and_box_deltas(batch_size, anchors, bboxes):
    rpn_cls = np.zeros((batch_size, len(anchors), 1))
    rpn_box_deltas = np.zeros((batch_size, len(anchors), 4))

    for i in range(len(bboxes)):
        classes, boxes = get_rpn_cls_and_box_deltas_for_image(anchors, bboxes[i])
        rpn_cls[i] = classes
        rpn_box_deltas[i] = boxes

    return rpn_cls, rpn_box_deltas

# calculate IoU between anchor box and gt bboxes
def calculate_iou(box, boxes, box_area, boxes_area):
    x1 = np.maximum(box[0], boxes[:, 0])
    y1 = np.maximum(box[1], boxes[:, 1])
    x2 = np.minimum(box[2], boxes[:, 2])
    y2 = np.minimum(box[3], boxes[:, 3])

    intersection = np.maximum(x2 - x1, 0) * np.maximum(y2 - y1, 0)

    union = box_area + boxes_area[:] - intersection[:]
    iou = intersection / union

    return iou

# calculate overlaps between given anchors (boxes1) and gt bboxes (boxes2)
def calculate_overlaps(boxes1, boxes2):
    # get xmin, xmax, ymin, ymax min and max anchor and gt bbox coordinate
    x1_min, y1_min, x1_max, y1_max = boxes1[:, 0], boxes1[:, 1], boxes1[:, 2], boxes1[:, 3]
    x2_min, y2_min, x2_max, y2_max = boxes2[:, 0], boxes2[:, 1], boxes2[:, 2], boxes2[:, 3]

    # get anchors and gt bboxes areas
    area1 = (x1_max - x1_min) * (y1_max - y1_min)
    area2 = (x2_max - x2_min) * (y2_max - y2_min)

    overlaps = np.zeros((boxes1.shape[0], boxes2.shape[0]))

    # for each anchor get IoU overlaps with gt bboxes
    for i in range(overlaps.shape[1]):
        b2 = boxes2[i]
        overlaps[:, i] = calculate_iou(b2, boxes1, area2[i], area1)

    return overlaps

# get classes and bounding box deltas given anchors and ground truth bounding boxes
def get_rpn_cls_and_box_deltas_for_image(anchors, bboxes_gt):
    np.random.seed(100)

    rpn_cls = np.zeros((len(anchors), 1))
    rpn_box_deltas = np.zeros((len(anchors), 4))

    overlaps = calculate_overlaps(anchors, bboxes_gt)

    anchor_iou_argmax = np.argmax(overlaps, axis=1)
    anchor_iou_max = overlaps[np.arange(overlaps.shape[0]), anchor_iou_argmax]
    rpn_cls[anchor_iou_max <= config.NEGATIVE_ANCHOR_THRESHOLD] = -1
    rpn_cls[anchor_iou_max >= config.POSITIVE_ANCHOR_THRESHOLD] = 1

    # set anchor for each gt bbox indepdendetly of IOU
    bbox_gt_iou_max = np.max(overlaps, axis=0)
    # take all of the anchors with the same max overlap with gt bbox
    rpn_cls[np.argwhere(overlaps == bbox_gt_iou_max)[:, 0]] = 1

    # sampling TRAIN_ANCHORS_PER_IMAGE anchors per image + balancing -1 and 1 anchor candidates (surplus of both positive and negative anchors is neutered to 0)
    pos_anc_inds = np.where(rpn_cls == 1)[0]
    extra = len(pos_anc_inds) - (config.TRAIN_ANCHORS_PER_IMAGE // 2)
    if extra > 0:
        pos_anc_inds = np.random.choice(pos_anc_inds, extra, replace=False)
        rpn_cls[pos_anc_inds] = 0

    neg_anc_inds = np.where(rpn_cls == -1)[0]
    extra = len(neg_anc_inds) - (config.TRAIN_ANCHORS_PER_IMAGE - np.sum(rpn_cls == 1))
    if extra > 0:
        neg_anc_inds = np.random.choice(neg_anc_inds, extra, replace=False)
        rpn_cls[neg_anc_inds] = 0

    # get all positive anchors and iterate over them
    anc_inds = np.where(rpn_cls == 1)[0]
    pos_ancs = anchors[anc_inds]
    for i, posAnc in zip(anc_inds, pos_ancs):
        # bbox_gt - anchor which has biggest overlap with the anchor (does not have to be IoU >= POSITIVE_ANCHOR_THRESHOLD)
        bbox_gt = bboxes_gt[anchor_iou_argmax[i]]

        # get width, height, x center, y center coordinates of gt bbox
        bbox_gt_w = bbox_gt[2] - bbox_gt[0]
        bbox_gt_h = bbox_gt[3] - bbox_gt[1]
        bbox_gt_xc = bbox_gt[0] + bbox_gt_w / 2
        bbox_gt_yc = bbox_gt[1] + bbox_gt_h / 2

        # get width, height, x center, y center coordinates of positive anchor
        pos_anc_w = posAnc[2] - posAnc[0]
        pos_anc_h = posAnc[3] - posAnc[1]
        pos_anc_xc = posAnc[0] + pos_anc_w / 2
        pos_anc_yc = posAnc[1] + pos_anc_h / 2

        # get deltas of anchor center in x, y directions
        dx = (bbox_gt_xc - pos_anc_xc) / pos_anc_w
        dy = (bbox_gt_yc - pos_anc_yc) / pos_anc_h

        # get dimension changes (relative change in width and height)
        dw = np.log(bbox_gt_w / pos_anc_w)
        dh = np.log(bbox_gt_h / pos_anc_h)

        rpn_box_deltas[i] = [dx, dy, dw, dh]

    return rpn_cls, rpn_box_deltas
