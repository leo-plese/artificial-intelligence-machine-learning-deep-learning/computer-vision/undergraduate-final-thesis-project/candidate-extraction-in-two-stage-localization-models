import os
from rpn_net import backbone, config, rpn
from rpn_net.utils import xml_util, image_util, anchor_util
import numpy as np
import torch
from sys import argv

# calculate AR1000 for models with weight files in ROOT_DIR and stores AR files in AR_FILES_DIR

torch.manual_seed(20)

model = rpn.RPN(backbone.Resnet34_FPN(), 3)
model.cuda()
anchors = anchor_util.get_anchors(config.IMAGE_SIZE, config.ANCHOR_SCALES, config.ANCHOR_RATIOS)

ROOT_DIR = argv[1] # root directory with subdirectories named same as image whose weights are contained within it (e.g. ROOT_DIR contains subdirectories "2008_000561" and "2007_000027" which contain weights for model trained on "2008_000561.jpg" and "2007_000027.jpg" respectively)
AR_FILES_DIR = argv[2] # root directory where for each image (with weight files given in ROOT_DIR) will be created one file of format "image_name.txt" e.g. "2008_000561.txt" would contain N lines with AR1000 values for each corresponding model i.e. weight file
for path, subdirs, files in os.walk(ROOT_DIR):
    if len(subdirs) == 0:
        print(path, subdirs, files)

        from re import fullmatch

        filenames = [x for x in range(0, config.EPOCHS_NUM + 1, 10)]

        files = sorted(files, key=lambda k: filenames.index(int(fullmatch(r"\D*(\d+)\.pth", k).groups()[0])))

        train_img_instance_name = path.split("\\")[-1]
        print(train_img_instance_name)

        path1_img = "..\\" + config.DATASET_FILE_PATH + "\\JPEGImages\\" + train_img_instance_name + ".jpg"
        path1_ann = "..\\" + config.DATASET_FILE_PATH + "\\Annotations\\" + train_img_instance_name + ".xml"

        img1 = image_util.load_image(path1_img)

        bboxes1 = xml_util.get_bboxes(path1_ann)

        write_ar_list = []
        for f in files:
            fn = os.path.join(path, f)
            print("file =", fn)

            with open(fn, "r") as descr:
                model.load_weights(fn)
                _, cls_softmaxes, bb_deltas = model(np.array([img1]))

                cls_softmaxes = cls_softmaxes.cpu().detach().numpy()
                bb_deltas = bb_deltas.cpu().detach().numpy()

                cs = cls_softmaxes[0]
                bs = bb_deltas[0]

                boxes_new = model.get_rpn_proposals(anchors, cs, bs, True)

                recalls = rpn.calculate_ar_recalls_over_iou(boxes_new, bboxes1)

                AR = recalls.mean()
                write_ar_list.append(str(AR.numpy()) + "\n")
                # print("recalls =", recalls)
                print("AR =", AR.numpy())
                print()

        with open(AR_FILES_DIR + "\\" + train_img_instance_name + ".txt",
                  "w") as ar_descr:
            ar_descr.writelines(write_ar_list)
