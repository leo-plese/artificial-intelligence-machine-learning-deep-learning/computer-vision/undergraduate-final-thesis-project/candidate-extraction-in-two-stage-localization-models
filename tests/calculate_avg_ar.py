import os
from rpn_net import config
from sys import argv

# calculate average AR1000 from AR files in ROOT_DIR

ROOT_DIR = argv[1] # root directory where for each of N images is stored one file of format "image_name.txt" e.g. "2008_000561.txt" would contain M lines with AR1000 values for each corresponding model i.e. weight file
ar_files = os.listdir(ROOT_DIR)
tot_files = len(ar_files)

ar_num = int(config.EPOCHS_NUM / 10)
# print("arNum =",arNum)
avg_ar = [0] * ar_num

for f in ar_files:
    f = os.path.join(ROOT_DIR, f)
    print("file =", f)

    with open(f, "r") as descr:
        lines = descr.readlines()
        lines = [l.strip() for l in lines]
        lines = [float(x) for x in lines if len(x) > 0]

        ars_enum = sorted(list(enumerate(lines)), key=lambda k: k[1], reverse=True)
        print(ars_enum)
        for i in range(ar_num):
            avg_ar[i] += lines[i]

avg_ar = [x / tot_files for x in avg_ar]
enum_avg_ar = sorted(list(enumerate(avg_ar)), key=lambda k: k[1], reverse=True)

for avg in enum_avg_ar:
    print(avg)
