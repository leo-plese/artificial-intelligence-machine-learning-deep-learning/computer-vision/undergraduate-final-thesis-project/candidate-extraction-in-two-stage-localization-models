from rpn_net import backbone, rpn, config
from rpn_net.utils import xml_util, image_util, anchor_util
import numpy as np
from PIL import Image, ImageDraw
import torch

# bounding boxes after learning deltas in dx, dy of center position and dw, dh of anchor dimensions
torch.manual_seed(20)

model = rpn.RPN(backbone.Resnet34_FPN(), 3)
model.cuda()

model.load_weights("..\\" + config.WEIGHTS_FILE_PATH)

anchors = anchor_util.get_anchors(config.IMAGE_SIZE, config.ANCHOR_SCALES, config.ANCHOR_RATIOS)

path1_img = "..\\" + config.DATASET_FILE_PATH + "\\JPEGImages\\" + config.TRAIN_IMAGE_INSTANCE_NAME + ".jpg"
path1_ann = "..\\" + config.DATASET_FILE_PATH + "\\Annotations\\" + config.TRAIN_IMAGE_INSTANCE_NAME + ".xml"

img1 = image_util.load_image(path1_img)
bboxes1 = xml_util.get_bboxes(path1_ann)

_, _, bb_deltas = model(np.array([img1]))

rpn_classes, _ = anchor_util.get_rpn_cls_and_box_deltas_for_image(anchors, bboxes1)

bb_deltas = bb_deltas.cpu().detach().numpy()
bs = bb_deltas[0]

inds = np.where(rpn_classes == 1)[0]
deltas = bs[inds]
boxes = anchors[inds]

h = boxes[:, 3] - boxes[:, 1]
w = boxes[:, 2] - boxes[:, 0]
yc = boxes[:, 1] + h / 2
xc = boxes[:, 0] + w / 2

yc += deltas[:, 1] * h
xc += deltas[:, 0] * w
h *= np.exp(deltas[:, 3])
w *= np.exp(deltas[:, 2])

y1 = yc - h / 2
x1 = xc - w / 2
y2 = y1 + h
x2 = x1 + w

boxes_new = []
for i in range(len(x1)):
    boxes_new.append([x1[i], y1[i], x2[i], y2[i]])

img = Image.open(path1_img).convert("RGBA")
draw_img = ImageDraw.Draw(img)
for i in range(len(boxes_new)):
    x1, y1, x2, y2 = boxes_new[i]
    draw_img.rectangle(((x1, y1), (x2, y2)), outline="red")

img.show()
