from rpn_net import backbone, rpn, config
from rpn_net.utils import xml_util, image_util, anchor_util
import numpy as np
from PIL import Image, ImageDraw
import torch

# case D in document - bounding boxes after NMS after learning - show top AFTER_NMS_SHOW_TOP_N
torch.manual_seed(20)

model = rpn.RPN(backbone.Resnet34_FPN(), 3)
model.cuda()

anchors = anchor_util.get_anchors(config.IMAGE_SIZE, config.ANCHOR_SCALES, config.ANCHOR_RATIOS)

path1_img = "..\\" + config.DATASET_FILE_PATH + "\\JPEGImages\\" + config.TRAIN_IMAGE_INSTANCE_NAME + ".jpg"
path1_ann = "..\\" + config.DATASET_FILE_PATH + "\\Annotations\\" + config.TRAIN_IMAGE_INSTANCE_NAME + ".xml"

img1 = image_util.load_image(path1_img)
bboxes1 = xml_util.get_bboxes(path1_ann)

##################
# calculate AR for each model given by training on image TRAIN_IMAGE_INSTANCE_NAME if VALIDATE_AR is set
if config.VALIDATE_AR:
    write_ar_list = []
    for epoch in range(10, config.EPOCHS_NUM + 1, 10):
        print("Epoch", epoch)
        model.load_weights("..\\" + config.WEIGHTS_FILE_PATH_BASE + str(epoch) + ".pth")
        cbs, cls_softmaxes, bb_deltas = model(np.array([img1]))

        cls_softmaxes = cls_softmaxes.cpu().detach().numpy()
        bb_deltas = bb_deltas.cpu().detach().numpy()

        cs = cls_softmaxes[0]
        bs = bb_deltas[0]

        boxes_new = model.get_rpn_proposals(anchors, cs, bs, True)

        recalls = rpn.calculate_ar_recalls_over_iou(boxes_new, bboxes1)

        AR = recalls.mean()
        write_ar_list.append(str(AR.numpy()) + "\n")
        # print("recalls =", recalls)
        print("AR =", AR.numpy())
        print()
######################

model.load_weights("..\\" + config.WEIGHTS_FILE_PATH)

_, cls_softmaxes, bb_deltas = model(np.array([img1]))

cls_softmaxes = cls_softmaxes.cpu().detach().numpy()
bb_deltas = bb_deltas.cpu().detach().numpy()

cs = cls_softmaxes[0]
bs = bb_deltas[0]

boxes_new = model.get_rpn_proposals(anchors, cs, bs, True)

#####
# show AR1000 for model from the last epoch of training (epoch nr. EPOCHS_NUM) on the image TRAIN_IMAGE_INSTANCE_NAME
recalls = rpn.calculate_ar_recalls_over_iou(boxes_new, bboxes1)

AR = recalls.mean()
print("recalls =", recalls)
print("AR =", AR)
#####

img = Image.open(path1_img).convert("RGBA")
draw_img = ImageDraw.Draw(img)
for i in range(config.AFTER_NMS_SHOW_TOP_N):
    x1, y1, x2, y2 = boxes_new[i]
    draw_img.rectangle(((x1, y1), (x2, y2)), outline="red")

img.show()
