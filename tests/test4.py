from rpn_net import backbone, rpn, config
from rpn_net.utils import xml_util, image_util, anchor_util
import numpy as np
from PIL import Image, ImageDraw
import torch

# case C in document - bounding boxes after learning (classes and bb deltas) - use of get_rpn_proposals function, show top BEFORE_NMS_SHOW_TOP_N proposals
torch.manual_seed(20)

model = rpn.RPN(backbone.Resnet34_FPN(), 3)
model.cuda()

model.load_weights("..\\" + config.WEIGHTS_FILE_PATH)

anchors = anchor_util.get_anchors(config.IMAGE_SIZE, config.ANCHOR_SCALES, config.ANCHOR_RATIOS)

path1_img = "..\\" + config.DATASET_FILE_PATH + "\\JPEGImages\\" + config.TRAIN_IMAGE_INSTANCE_NAME + ".jpg"

img1 = image_util.load_image(path1_img)

_, cls_softmaxes, bb_deltas = model(np.array([img1]))

cls_softmaxes = cls_softmaxes.cpu().detach().numpy()
bb_deltas = bb_deltas.cpu().detach().numpy()
cs = cls_softmaxes[0]
bs = bb_deltas[0]

boxes_new = model.get_rpn_proposals(anchors, cs, bs)

img = Image.open(path1_img).convert("RGBA")
draw_img = ImageDraw.Draw(img)
for i in range(config.BEFORE_NMS_SHOW_TOP_N):
    x1, y1, x2, y2 = boxes_new[i]
    draw_img.rectangle(((x1, y1), (x2, y2)), outline="red")

img.show()
