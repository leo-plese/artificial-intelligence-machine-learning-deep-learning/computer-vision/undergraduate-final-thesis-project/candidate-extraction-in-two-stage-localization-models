from rpn_net import config
from rpn_net.utils import xml_util, image_util, anchor_util
import numpy as np
from PIL import Image, ImageDraw
import torch

# case A in document - ground truth bounding bboxes
torch.manual_seed(20)

anchors = anchor_util.get_anchors(config.IMAGE_SIZE, config.ANCHOR_SCALES, config.ANCHOR_RATIOS)

path1_img = "..\\" + config.DATASET_FILE_PATH + "\\JPEGImages\\" + "2011_004761" + ".jpg"
path1_ann = "..\\" + config.DATASET_FILE_PATH + "\\Annotations\\" + "2011_004761" + ".xml"

img1 = image_util.load_image(path1_img)
bboxes1 = xml_util.get_bboxes(path1_ann)

rpn_classes, rpn_bb_deltas = anchor_util.get_rpn_cls_and_box_deltas_for_image(anchors, bboxes1)

inds = np.where(rpn_classes == 1)[0]
bb_deltas = rpn_bb_deltas[inds]
boxes = anchors[inds]

h = boxes[:, 3] - boxes[:, 1]
w = boxes[:, 2] - boxes[:, 0]
yc = boxes[:, 1] + h / 2
xc = boxes[:, 0] + w / 2

yc += bb_deltas[:, 1] * h
xc += bb_deltas[:, 0] * w
h *= np.exp(bb_deltas[:, 3])
w *= np.exp(bb_deltas[:, 2])

y1 = yc - h / 2
x1 = xc - w / 2
y2 = y1 + h
x2 = x1 + w

boxes_gt = []
for i in range(len(x1)):
    boxes_gt.append([x1[i], y1[i], x2[i], y2[i]])

img = Image.open(path1_img).convert("RGBA")
draw_img = ImageDraw.Draw(img)
for i in range(len(boxes_gt)):
    x1, y1, x2, y2 = boxes_gt[i]
    draw_img.rectangle(((x1, y1), (x2, y2)), outline="red")

img.show()
