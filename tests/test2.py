from rpn_net import config
from rpn_net.utils import xml_util, image_util, anchor_util
import numpy as np
from PIL import Image, ImageDraw
import torch

# case B in document - ground truth bounding with IoU >= 0.6
torch.manual_seed(20)

anchors = anchor_util.get_anchors(config.IMAGE_SIZE, config.ANCHOR_SCALES, config.ANCHOR_RATIOS)

path1_img = "..\\" + config.DATASET_FILE_PATH + "\\JPEGImages\\" + config.TRAIN_IMAGE_INSTANCE_NAME + ".jpg"
path1_ann = "..\\" + config.DATASET_FILE_PATH + "\\Annotations\\" + config.TRAIN_IMAGE_INSTANCE_NAME + ".xml"

img1 = image_util.load_image(path1_img)
bboxes1 = xml_util.get_bboxes(path1_ann)

rpn_classes, rpn_bb_deltas = anchor_util.get_rpn_cls_and_box_deltas_for_image(anchors, bboxes1)

inds = np.where(rpn_classes == 1)[0]
boxes = anchors[inds]

img = Image.open(path1_img).convert("RGBA")
draw_img = ImageDraw.Draw(img)
for i in range(len(boxes)):
    x1, y1, x2, y2 = boxes[i]
    draw_img.rectangle(((x1, y1), (x2, y2)), outline="red")

img.show()
